import {
   FETCH_USERS,
   FETCH_USERS_SUCCESS,
   FETCH_USERS_FAILURE,
} from '../actions/types.js';

const initialState = {
   users: [],
   errors: [],
   loading: true,
}
const reducer = (state = initialState, action) => {
   switch(action.type) {
      case FETCH_USERS:
         return {
            ...state,
            loading: true,
            errors: [],
         };
      case FETCH_USERS_SUCCESS:
         return {
            ...state,
            users: action.users,
            loading: false,
            errors: [],
         };
      case FETCH_USERS_FAILURE:
         return {
            ...state,
            users: [],
            loading: false,
            errors: action.errors,
         };
      default: 
         return state;
   }
}

export default reducer;