import {
   FETCH_USERS,
   FETCH_USERS_SUCCESS,
   FETCH_USERS_FAILURE,
} from './types.js';

export const fetchUsers = () => {
   return {
      type: FETCH_USERS,
   }
}

export const fetchUsersSuccess = users => {
   return {
      type: FETCH_USERS_SUCCESS,
      users,
   }
}

export const fetchUsersFailure = errors => {
   return {
      type: FETCH_USERS_FAILURE,
      errors,
   }
}
