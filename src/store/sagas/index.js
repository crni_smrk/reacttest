import { all, takeEvery } from 'redux-saga/effects';
import { FETCH_USERS } from '../actions/types';
import { fetchUsersSaga } from './user';

export function* watchUsers() {
   yield all([
      yield takeEvery(FETCH_USERS, fetchUsersSaga),
   ]);
}