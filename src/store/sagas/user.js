import { put, call } from 'redux-saga/effects';
import * as actions from '../actions/index';

const url = 'https://iunaptk810.execute-api.ap-southeast-1.amazonaws.com/dev/api/users';

export function* fetchUsersSaga(action) {
   try {
      const response = yield call(fetch, url, {
         method: 'get',
         headers: {
            "Content-type": "application/json"
         },
      });
      if(!response.ok) {
         throw new Error('Network response was not ok');
      }
      const json = yield response.json();
      yield put(actions.user.fetchUsersSuccess(json));
   } catch (e) {
      yield put(actions.user.fetchUsersFailure([e.message]));
   }
}