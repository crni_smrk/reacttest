import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { Provider } from 'react-redux';
import usersReducer from './store/reducers/user';
import { watchUsers } from './store/sagas/index';

import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';

const composeEnhancers = (process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null) || compose;


const sagaMiddleware = createSagaMiddleware();

const rootReducer = combineReducers({
   users: usersReducer,
});

const store = createStore(
   rootReducer,
   composeEnhancers(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(watchUsers);

const app = (
   <Provider store={store} >
      <BrowserRouter>
         <App />
      </BrowserRouter>
   </Provider>
);

ReactDOM.render(
   app,
   document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
