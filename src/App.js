import React from 'react';
import Button from 'react-bootstrap/Button';
import { useDispatch } from 'react-redux';

import * as actions from './store/actions/index';
import InviteUser from './components/user/InviteUser';

import './App.scss';


function App() {
   const dispatch = useDispatch();

   const clickHandler = () => {
      dispatch(actions.user.fetchUsers());
   }

   return (
      <div className="App">
         {/* <Button type='button' onClick={clickHandler}>Invite a Friend</Button> */}
         <InviteUser />
      </div>
   );
}

export default App;
