import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Form from 'react-bootstrap/Form';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Button from 'react-bootstrap/Button';

import UsersList from './UsersList';
import * as actions from '../../store/actions/index';
import classes from './InviteUser.module.scss';

const InviteUser = props => {

   const [filter, setFilter] = useState('');
   const [activeTab, setActiveTab] = useState('Desainer');
   const [selectedUsers, setSelectedUsers] = useState([]);
   const users = useSelector(state => state.users.users);
   const dispatch = useDispatch();

   const filteredUsers = users.filter(user => user.name.toLowerCase().includes(filter.toLowerCase()));
   const suggestedUsers = filteredUsers.filter(user => user.isActive);

   useEffect(() => {
      dispatch(actions.user.fetchUsers());
   }, [dispatch]);
   const changeHandler = evt => {
      setFilter(evt.target.value);
   }


   return (
      <div className={classes.InviteUser}>
         <h3>Undang Peserta Terbaik untuk Tim Baru mu!</h3>
         <Form>
            <Form.Group>
               <Form.Control type="text" onChange={changeHandler} value={filter} />
            </Form.Group>
         </Form>
         <Tabs
            id="invite-user-tabs"
            activeKey={activeTab}
            onSelect={(k) => setActiveTab(k)}
         >
            <Tab eventKey="Desainer" title="Desainer">
               <UsersList users={suggestedUsers} listTitle="SUGGESTIONS" jobPosition={activeTab} />
               <UsersList users={filteredUsers} listTitle="ALL MEMBERS" jobPosition={activeTab} />
            </Tab>
            <Tab eventKey="Guru" title="Guru">
               <UsersList users={suggestedUsers} listTitle="SUGGESTIONS" jobPosition={activeTab} />
               <UsersList users={filteredUsers} listTitle="ALL MEMBERS" jobPosition={activeTab} />
            </Tab>
         </Tabs>
         <Button disabled={!selectedUsers.length}>Undang Bergabung ({selectedUsers.length})</Button>
      </div>
   );

}

export default InviteUser;
