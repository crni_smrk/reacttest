import React from 'react';
import UserCard from './UserCard';

const UsersList = ({ users, listTitle, jobPosition }) => {

   const userList = users.map(user => (
      <UserCard user={user} key={user._id} jobPosition={jobPosition} />
   ));

   return (
      <>
         <h4>{listTitle}</h4>
         {userList}
      </>
   );

}

export default UsersList;
