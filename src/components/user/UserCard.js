import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';

import classes from './UserCard.module.scss';

const UserCard = ({ user, jobPosition }) => {
   let tags = '';
   user.tags.forEach(tag => {
      tags += ' & ' + tag;
   });
   
   const description = (
      <p className={classes.UserDescripition}>{jobPosition} {user.company} <span> {tags.substring(2)} </span></p>
   );
   return (
      <Container fluid className={classes.UserCard}>
         <Row>
            <Col xs={2}>
               <Image src={user.picture} fluid roundedCircle />
            </Col>
            <Col xs={10}>
               <Row>
                  <Col>
                     <h4 className={classes.UserName} >{user.name}</h4>
                  </Col>
               </Row>
               <Row>
                  {description}
               </Row>
               <Row>

               </Row>
            </Col>
         </Row>
      </Container>
   );

}

export default UserCard;
